import { readFile, writeFile, existsSync, readFileSync, readdirSync, createReadStream, writeFileSync } from 'fs';
import { emptyDirSync, copy } from 'fs-extra';
import opn from 'open';
import { lookup } from 'mime-types';
import { createInterface } from 'readline';
import { google, drive_v3 } from 'googleapis';

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/drive'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

// Launch the local backup
// If process.argv[3] is null, then cancel the backup.
// console.log(copy_folder());
if (!(process.argv[3] === undefined)) {
  copy_folder()
  console.log("Copiando en local backup...");
}

// Load client secrets from a local file.
readFile('credentials.json', (err, content) => {
  if (err) return console.log('Error loading client secret file:', err);
  // Authorize a client with credentials, then call the Google Drive API.
  authorize(JSON.parse(content), uploadAFile); // Upload all the files on the folder
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  readFile(TOKEN_PATH, (err, token) => {
    if (err) return getAccessToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getAccessToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  opn(authUrl);
  const rl = createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

/**
 * Upload files recursively
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
async function uploadAFile(auth, ruta, carpetaPadre, nombre) {
  if (ruta == null) ruta = process.argv[2]; // El calback no lo establece
  const drive = google.drive({ version: 'v3', auth });
  if (carpetaPadre == null) {
    //  TODO: Crear una carpeta padre si no existe una en el json

    if (!existsSync('folderid.json')) {
      // File does not exit, creating folder
      let googleDownload = await create_folder(drive)
      console.log(googleDownload);
    }
    var datetime = new Date();
    const nombreNuevaCarpeta = "Copia de seguridad " + datetime.toISOString().slice(0, 10);
    var rawNewJson = readFileSync('folderid.json');
    var newJson = JSON.parse(rawNewJson);
    var folderParentId = newJson.id;
    var fileMetadata = {
      'name': [nombreNuevaCarpeta],
      'mimeType': 'application/vnd.google-apps.folder',
      parents: [folderParentId]
    };
  } else {
    fileMetadata = {
      'name': [nombre],
      'mimeType': 'application/vnd.google-apps.folder',
      parents: [carpetaPadre]
    };
  }
  drive.files.create({ // Create the folder for the upload
    resource: fileMetadata,
    fields: 'id'
  }, function (err, file) {
    if (err) {
      console.error("Reintentando por rate limit");
    } else {
      console.log('Folder id: ', file.data.id);
      var folderId = file.data.id;
      const dir = readdirSync(ruta, { withFileTypes: true }); // Leer la carpeta
      dir.forEach(file => { // Ir por cada fichero de la carpeta subiendolo a drive
        var fileName = file.name;
        if (file.isDirectory()) {
          console.log("Detectada carpeta");
          var newRuta = ruta + '/' + fileName;
          console.log("Nueva ruta: ", newRuta)
          uploadAFile(auth, newRuta, folderId, fileName);
        } else {
          var fileMetadata = {
            'name': file.name,
            parents: [folderId]
          };
          var mimeType = lookup(file.path)
          var media = null;
          if (mimeType == null) {
            console.error("El tipo mime es null. No se conoce este formato, intentando con binario");
            media = {
              mimeType: 'application/octet-stream',
              body: createReadStream(ruta + '/' + file.name)
            };
          } else {
            media = {
              mimeType: mimeType,
              body: createReadStream(ruta + '/' + file.name)
            };
          } drive.files.create({
            resource: fileMetadata,
            media: media,
            fields: 'id'
          }, function (err) {
            if (err) {
              // Handle error
              console.error("Reintentando por rate limit");
            } else {
              console.log('Uploaded: ', fileName);
            }
          });
        }
      })
    }
  })
}

/**
 * Create a folder in google drive
 * @param {drive_v3.Drive} drive An authorized Drive client.
 */
async function create_folder(drive) {
  return new Promise((resolve, reject) => {
    var fileMetadata = {
      'name': 'Cirmax',
      'mimeType': 'application/vnd.google-apps.folder'
    };

    drive.files.create({
      resource: fileMetadata,
      fields: 'id'
    }, function (err, file) {
      if (err) return reject(err);
      let folderId = file.data.id;
      console.log("New Sync folder created. ID: ", folderId);
      var JsonDataFolder = JSON.parse("{}")
      JsonDataFolder.id = folderId;
      writeFileSync('folderid.json', JSON.stringify(JsonDataFolder));
      return resolve('folder created.');
    });
  });
}

async function copy_folder() {
  return new Promise((resolve, reject) => {
    var datetime = new Date();
    let newCopy = process.argv[3] + "/Copia de seguridad " + datetime.toISOString().slice(0, 10);
    if (existsSync(newCopy)) {
      emptyDirSync(newCopy);
    }
    copy(process.argv[2], newCopy, { overwrite: true }, err => {
      if (err) return reject(err);
      console.log('Local backup done.')
      resolve("Done local backup");
    })
  });
}