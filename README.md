# Google Drive BackUp program

## Usage

You can either use node js or the compiled binaries

### Node js
```bash
node src/index.js {folder to backup}
```

### Using exe
```bash
./drivebackup_xXX.exe {folder to backup}
```

## Building

You can build this program using pkg.

## Author

Aitor Ruiz Garcia
